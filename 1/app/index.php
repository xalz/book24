<?php

require __DIR__ . '/bootstrap.php';

/**
 * Получение репозитория
 * @var $productRepository \Doctrine\Common\Persistence\ObjectRepository
 */
$productRepository = $entityManager->getRepository(App\Entity\Product::class);

/**
 * Получения данных из БД
 * @var $product \App\Entity\Product
 */
$product = $productRepository->find(1);

/**
 * Обновление данных
 */
$product->setNomCode('new nomCode');
$product->setName('new name');

/**
 * Сохранение различий в пямять т.к. после flush переменная ChangeSet очищается
 * Так же можно перехватить евент onFlush и вывести различия там.
 * Писать свою логику нахождения различия считаю не целисообразным
 */
$uow = $entityManager->getUnitOfWork();
$uow->computeChangeSet($entityManager->getClassMetadata(get_class($product)), $product);
$change = $uow->getEntityChangeSet($product);

/**
 * Коммит изменений
 * Модель обновляется и доступа к оригинальным данным у нас больше нет
 */
$entityManager->flush();


/**
 * Вывод различий.
 */
var_dump($change);

