<?php

include __DIR__ . '/vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

$paths = [
    __DIR__ . '/src/Entity'
];

$isDevMode = false;

$dbParams = [
    'driver' => 'pdo_mysql',
    'host' => 'mysql',
    'user' => 'root',
    'password' => '',
    'dbname' => 'test',
];

$reader = new AnnotationReader();
$cache = new ArrayCache();
$driver = new AnnotationDriver($reader, $paths);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$config->setMetadataCacheImpl($cache);
$config->setQueryCacheImpl($cache);
$config->setMetadataDriverImpl($driver);

$entityManager = EntityManager::create($dbParams, $config);