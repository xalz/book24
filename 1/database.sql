CREATE TABLE `products` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nomCode` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    `isNew` tinyint(1) DEFAULT '1',
    PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `products` VALUES ('1', 'old nomCode', 'old name', '1');