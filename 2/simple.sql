--
-- Таблица книг
--
CREATE TABLE `books` (
    `book_id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `description` varchar(255) NOT NULL,
    `status` int(11) NOT NULL,
    `date` timestamp NOT NULL,
    PRIMARY KEY (`book_id`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Таблица авторов
--
CREATE TABLE `authors` (
     `author_id` int(11) NOT NULL AUTO_INCREMENT,
     `name` varchar(255) NOT NULL,
      PRIMARY KEY (`author_id`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Таблица связей
--
CREATE TABLE `books_authors` (
    `book_id` int(11) NOT NULL,
    `author_id` int(11) NOT NULL
) DEFAULT CHARSET=utf8;

--
-- Индексы
--
CREATE INDEX books_authors_author_id_index ON books_authors(author_id);
CREATE INDEX books_authors_book_id_index ON books_authors(book_id);


--
-- Запрос на получение книг
--
SELECT books.book_id, books.title, books.description, books.status, books.date
FROM books
WHERE books.status = 2
ORDER BY books.date DESC
LIMIT 10 OFFSET 0;

--
-- Запрос на получение авторов из полученных книг, где IN(...) перечисление book_id
--
SELECT authors.author_id, authors.name, books_authors.book_id
FROM authors
INNER JOIN books_authors ON authors.author_id = books_authors.author_id
WHERE books_authors.book_id IN(...)

--
-- Так же при запросе связей необходимо учесть сортировку из первого запроса
-- Зачастую ORM это делает самостоятельно
-- ORDER BY FIELD(books_authors.book_id, ...)
--
;